package example

import (
	"io/ioutil"
	"log"
	"net/http"
	"os"

	fs "gitlab.com/ds-go/data/fs"
)

func main() {
	//add custom ext so files are handled correctly
	//mime.AddExtensionType(ext, type)

	k, _ := os.Open("user.key")
	key, _ := ioutil.ReadAll(k)

	//Create new store with key from user.key
	d := fs.NewStore(string(key))

	//Populate store with user files
	//LoadGoData from output.go
	LoadGoData(d)

	gofs := fs.NewDataFileSystem(d)

	log.Fatal(http.ListenAndServe(":80", http.FileServer(gofs)))
}

//Provided om export
func LoadGoData(d *fs.Datastore) {
	return
}
