package fs

import (
	"errors"
	"io"
	"net/http"
	"os"
)

type FileSystem struct {
	*DataStore
}

func NewDataFileSystem(data *DataStore) *FileSystem {
	return &FileSystem{
		data,
	}
}

func (fs *FileSystem) Open(name string) (http.File, error) {
	info, err := fs.GetContents(name)
	if err != nil {
		return nil, err
	}
	file := &File{
		info: info,
		open: true,
		pos:  0,
		dir:  make([]os.FileInfo, 0),
	}
	if info.IsDir() {
		list, err := fs.GetDirInfo(info.Path)
		if err != nil {
			return nil, err
		}
		file.dir = list
	}
	return file, nil
}

type File struct {
	info *FileInfo
	pos  int
	open bool
	dir  []os.FileInfo
}

func (f *File) Close() error {
	f.open = false
	f.pos = 0
	return nil
}

func (f *File) Stat() (os.FileInfo, error) {
	return f.info, nil
}

func (f *File) Readdir(count int) ([]os.FileInfo, error) {
	if f == nil {
		return nil, os.ErrNotExist
	}
	if !f.info.IsDir() {
		return nil, errors.New("cannot read non dir")
	}
	return f.dir, nil
}

func (f *File) Read(p []byte) (n int, err error) {
	filesize := len(f.info.Bytes)
	n = len(p)
	if (filesize - f.pos) > n {
		n = copy(p, f.info.Bytes[f.pos:f.pos+n])
		f.pos += n
		return
	}
	n = copy(p, f.info.Bytes[f.pos:f.pos+n])
	err = io.EOF
	return
}

func (f *File) Seek(offset int64, whence int) (int64, error) {
	var size int64
	// Whence is the point of reference for offset
	// 0 = Beginning of file
	// 1 = Current position
	// 2 = End of file
	switch whence {
	case 0:
		size = int64(len(f.info.Bytes[:offset]))
		return size, nil
	case 1:
		pos := int(f.pos)
		off := int(offset)
		max := off + pos
		if len(f.info.Bytes) < max {
			return int64(len(f.info.Bytes[pos:])), io.EOF
		}
		size = int64(len(f.info.Bytes[pos:off]))
		return size, nil
	case 2:
		data := f.info.Bytes
		size = int64(len(data[len(f.info.Bytes)-int(offset):]))
		return size, nil
	default:
		return 0, errors.New("whence not valid")
	}
}
