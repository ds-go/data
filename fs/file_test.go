package fs

import (
	"testing"
	"time"
)

func MockFile() *FileInfo {
	return &FileInfo{
		name:        "name",
		size:        1000,
		dir:         false,
		modtime:     time.Time{},
		mode:        100,
		Bytes:       []byte("test"),
		ContentType: "contentType",
		Path:        "path/to/file",
		Parent:      "parent",
	}
}

func Test_Name(t *testing.T) {
	f := MockFile()
	if f.Name() != "name" {
		t.Error("name not equal")
	}
}

func Test_Size(t *testing.T) {
	f := MockFile()
	if f.Size() != 1000 {
		t.Error("size not equal")
	}
}
func Test_IsDir(t *testing.T) {
	f := MockFile()
	if f.IsDir() != false {
		t.Error("IsDir not equal")
	}
}
func Test_Sys(t *testing.T) {
	f := MockFile()
	if f.Sys() != nil {
		t.Error("Sys not equal")
	}
}
func Test_Mode(t *testing.T) {
	f := MockFile()
	if f.Mode() != f.mode {
		t.Error("Mode not equal")
	}
}
