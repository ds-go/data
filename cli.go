package main

import (
	"flag"
	"fmt"

	fs "gitlab.com/ds-go/data/fs"
)

type StringBoolFlag struct {
	set   bool
	value string
}

var (
	importFlag StringBoolFlag
	exportFile StringBoolFlag

	helpFlag  bool
	exportLoc string
)

func (sf *StringBoolFlag) Set(x string) error {
	sf.value = x
	sf.set = true
	return nil
}

func (sf *StringBoolFlag) String() string {
	return sf.value
}

func ParseCLIFlags() {
	flag.BoolVar(&helpFlag, "help", false, "go-data help")
	flag.Var(&importFlag, "import", "the directory to import")
	flag.StringVar(&exportLoc, "export", "enc", "the directory to export to")
	flag.Var(&exportFile, "o", "the output go file")
	flag.Parse()
}

func cliexport(path string, output string) {
	fmt.Println("new dir", path, output)
	datastore := fs.NewStore(fs.GenKey(64))

	err := datastore.InfoFromPath(path)
	if err != nil {
		fmt.Println("there was an error")
		fmt.Println(err)
		return
	}

	fmt.Println("Set Import dir:", path)
	fmt.Println("Set Export dir:", exportLoc)

	err = export(output, exportLoc, datastore)

	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Printf("export complete. path:[%v] has been saved to: %v \n", path, output)
}

func cliHeader() {
	fmt.Println(`
Go Data v1.0.0
Bind files to binary and access using file system.

-import		Directory to import
-export		Directory to export
-o			Output go file
-help		Go-Data help` + "\n")
}

func cliHelp() {
	fmt.Printf(`

Go Data compresses (GZip) and Encrypts (CFBEncrypt) files
and is accessible from its own filesystem. Files are stored
encrypted to the host filesystem and decrypts to memory on 
file access.

Examples can be seen in the example folder.

Exporting files

Example:
./go-data -import ./import-folder -export ./export-folder -o output.go

Exporting accepts the folling flags:

-import: [./import-folder] the folder to encode
-export: [./exported-folder] the folder to save encoded files to be referenced
-o:      [output.go] the name of the go file to import into projects

Export creates the following files:

./exported-folder:  Contains encoded files to be included in project
/output.go:         Go file to be included in project with Datastore.
					output.go uses main namespace.

user.key: generated key to be imported into project` + "\n\n")
}
