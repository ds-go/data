package main

const (
	templateHeader = `package main

import (
	"time"
	fs "gitlab.com/krobolt/go-data/fs"
)
	
func LoadGoData(dest *fs.DataStore){	
	`
	templateDirRoot = `dest.Path = "%s"
	var rec *fs.FileInfo
	`
	templateDir = `
	dest.Root["%s"] = &fs.Directory{
		Name: "%s",
		Path: "%s",
		Contents: make(map[string]*fs.FileInfo),
	}
	`
	templateContents = `
	rec = fs.CreateFileInfo("%s", %v, %#v, %v, time.Unix(%v, 0))
	rec.Path = "%s"
	rec.Bytes =  %#v
	rec.ContentType = "%s"
	rec.Parent = "%s"
	dest.Root["%s"].Contents["%v"] = rec
	`
	templateEnd = `
}`
)
